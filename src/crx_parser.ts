export class CrxParser {
    private _formatUint32(uint: number): string {
        var str = uint.toString(16);
        while (str.length < 8) {
            str = '0' + str;
        }
        return '0x' + str;
    }

    private _formatCharString(uint: number): string {
        var str = this._formatUint32(uint);
        str = str.substr(2, 8);
        var o = '';
        for (var i = 0; i < 4; i++) {
            o += String.fromCharCode(parseInt(str.substr(i << 1, 2), 16));
        }
        return o;
    }

    public parse(arrayBuffer: ArrayBuffer, dataView: DataView): ArrayBuffer | number {
        let magic = dataView.getUint32(0);
        if (magic != 0x43723234) return 1;

        let version = dataView.getUint32(4);

        if (version <= 2) {
            let publicKeyLength = dataView.getUint32(8, true);
            let signatureLength = dataView.getUint32(12, true);

            let zipArchiveBuffer = arrayBuffer.slice(16 + publicKeyLength + signatureLength);

            return zipArchiveBuffer;
        } else {
            let headerLength = dataView.getUint32(8, true);
            let zipArchiveBuffer = arrayBuffer.slice(12 + headerLength);

            return zipArchiveBuffer;
        }
    }

    public isCRXFile(dataView: DataView): boolean {
        let magic = dataView.getUint32(0);
        if (magic == 0x43723234) return true;
        else return false;
    }
}