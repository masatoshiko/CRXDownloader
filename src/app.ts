import { MDCRipple } from '@material/ripple';
import { MDCTextField } from '@material/textfield';
import { Dialog } from "./dialog";
import { CrxParser } from "./crx_parser";

let crxDownloadBaseUrl: string = 'https://clients2.google.com/service/update2/crx?response=redirect&prodversion=49.0&acceptformat=crx3&x=id%3D***%26installsource%3Dondemand%26uc';

window.onload = function() {
    initMDC();

    let fileInput: HTMLInputElement = <HTMLInputElement>document.getElementById("fileInput");
    let downloadButtonElement: HTMLElement = document.getElementById("downloadSourceButton")!;
    let selectedFileNameElement: HTMLElement = document.getElementById("selected-file")!;
    fileInput.addEventListener("change", async () => {
        downloadButtonElement.removeAttribute("data-download-url");
        downloadButtonElement.removeAttribute("data-download-name");
        downloadButtonElement.setAttribute("disabled", "");

        let files = fileInput.files;
        if (files == null) {
            selectedFileNameElement.textContent = "ファイルが選択されていません";
            return;
        } else selectedFileNameElement.textContent = files[0].name;

        let reader = new FileReader();
        reader.onload = ((e) => {
            let crx_parser = new CrxParser();
        
            let buffer: string | ArrayBuffer | null = e.target!.result;
            if (buffer == null) {
                Dialog.showErrorDialog("バッファがありません(nullです)。", null);
                return;
            }
            let dataView = new DataView(<ArrayBuffer>buffer);
        
            if (crx_parser.isCRXFile(dataView) == false) {
                Dialog.showErrorDialog("ファイルがChrome拡張機能(.crx)ではありません。", "エラー - ファイルが正しくありません");
                return;
            }
        
            let archiveBuffer: ArrayBuffer | number = crx_parser.parse(<ArrayBuffer>buffer, dataView);
            if (typeof(archiveBuffer) === 'number') {
                if (archiveBuffer == 1) Dialog.showErrorDialog("ファイルがChrome拡張機能(.crx)ではありません。", "エラー - ファイルが正しくありません");
                return;
            } else {
                let outputFile = new Blob([archiveBuffer], { type: "application/zip" });
                downloadButtonElement.setAttribute("data-download-url", URL.createObjectURL(outputFile));
                downloadButtonElement.setAttribute("data-download-name", `${files![0].name}.zip`);
            }

            downloadButtonElement.removeAttribute("disabled");
        });

        reader.readAsArrayBuffer(files[0]);
    });
}

function initMDC() {
    let mdcButtonElement: NodeListOf<HTMLElement> = document.querySelectorAll('.mdc-button');
    let mdcTextFieldElement: NodeListOf<HTMLElement> = document.querySelectorAll('.mdc-text-field');

    mdcTextFieldElement.forEach(element => MDCTextField.attachTo(element));
    mdcButtonElement.forEach(element => {
        let btnName: string | null = element.getAttribute("data-btn-name");
        MDCRipple.attachTo(element);

        switch (btnName) {
            case "getDownloadUrl":
                element.addEventListener("click", () => {
                    let inputValue: string = getInputValue("#webstore-url-input");
                    let result: number = isWebstoreUrl(inputValue);
                    let errorMessages: string[] = ["Chrome ウェブストアのURLではありません。", "拡張機能IDが取得できませんでした。"];

                    if (result == 0) {
                        let extensionID: string = getExtensionID(inputValue);
                        Dialog.showGetDownloadUrlDialog(getExtensionDownloadUrl(extensionID));
                    }
                    else Dialog.showErrorDialog(errorMessages[result - 1], null);
                });
                break;

            case "downloadCRX":
                element.addEventListener("click", () => {
                    let inputValue: string = getInputValue("#webstore-url-input");
                    let result: number = isWebstoreUrl(inputValue);
                    let errorMessages: string[] = ["Chrome ウェブストアのURLではありません。", "拡張機能IDが取得できませんでした。"];

                    if (result == 0) {
                        let extensionID: string = getExtensionID(inputValue);
                        location.href = getExtensionDownloadUrl(extensionID);
                    }
                    else Dialog.showErrorDialog(errorMessages[result - 1], null);
                });
                break;
        
            case "downloadSource":
                element.addEventListener("click", () => {
                    let downloadUrl: string | null = element.getAttribute("data-download-url");
                    let downloadName: string | null = element.getAttribute("data-download-name");

                    if (downloadUrl != null && downloadName != null) download(downloadName, downloadUrl);
                });
            default:
                break;
        }
    });
}

function getInputValue(selector: string) {
    let inputElement: HTMLInputElement | null = <HTMLInputElement>document.querySelector(selector);
    if (inputElement == null) throw "指定されたinput要素がありません。";
    else return inputElement.value;
}

function getExtensionID(url: string): string {
    let extensionID: string = url.split("/")[6];
    return extensionID.split("?")[0];
}

function getExtensionDownloadUrl(extensionID: string): string {
    return crxDownloadBaseUrl.replace("***", extensionID);
}

function isWebstoreUrl(url: string): number {
    if (!url.startsWith("https://chrome.google.com/webstore/") && !url.startsWith("http://chrome.google.com/webstore/")) return 1;
    let splited_url: string[] = url.split("/");

    console.log(splited_url[6]);
    if (splited_url.length < 6) return 2;
    else if (splited_url[6] == "") return 2;
    else return 0;
}

function download(filename: string, url: string) {
    let dl_element: HTMLElement = document.createElement("a");
    dl_element.setAttribute("href", url);
    dl_element.setAttribute("download", filename);

    document.body.appendChild(dl_element);
    dl_element.click();

    document.body.removeChild(dl_element);
}