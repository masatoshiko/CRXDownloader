import { MDCDialog } from '@material/dialog';

export class Dialog {
    public static showGetDownloadUrlDialog(url: string) {
        let getDownloadUrlDialog: MDCDialog = new MDCDialog(document.getElementById("getDownloadUrlDialog")!);
        let downloadUrlElement: HTMLElement = document.getElementById("downloadUrl")!;

        downloadUrlElement.textContent = url;
        downloadUrlElement.setAttribute("href", url);
        downloadUrlElement.style.overflowX = "scroll";
        console.log(url);
        getDownloadUrlDialog.open();
    }

    public static showErrorDialog(errorText: string, title: string | null) {
        let errorDialog: MDCDialog = new MDCDialog(document.getElementById("errorDialog")!);
        let errorDialogTitleElement: HTMLElement = document.getElementById("errorDialog-title")!;
        let errorDialogTextElement: HTMLElement = document.getElementById("errorDialog-content")!;

        errorDialogTitleElement.textContent = title ?? "エラー";
        errorDialogTextElement.textContent = errorText;
        errorDialog.open();
    }
}