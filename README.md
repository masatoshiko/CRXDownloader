# CRX Downloader
Chrome拡張機能(.crx)を直接ダウンロードできるツールです。

必要であれば、ソースに変換することもできます。

## 謝辞
以下のOSSのプログラムを使用・参考して作成しました。

- [material-components/material-components-web](https://github.com/material-components/material-components-web)
- [vladignatyev/crx-extractor](https://github.com/vladignatyev/crx-extractor) - CRX関係のコードを参考